REGISTRY ?= tsunny
IMAGE=requestbin
TAG ?= latest


build-image:
	docker build -t $(REGISTRY)/$(IMAGE):$(TAG) -f Dockerfile .

push-image: build-image
	docker push $(REGISTRY)/$(IMAGE):$(TAG)

